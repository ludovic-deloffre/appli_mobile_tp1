package com.example.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import data.Country;

import static data.Country.countries;

public class DetailFragment extends Fragment {


    TextView itemTitle;
    TextView itemLangue;
    TextView itemMonnaie;
    TextView itemPopulation;
    TextView itemSuperficie;
    TextView itemCapitale;
    ImageView itemImage;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int i = args.getCountryId();

        String uri = Country.countries[i].getImgUri();

        itemImage = view.findViewById(R.id.item_image);

        Context c = itemImage.getContext();
        itemImage.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri , null , c.getPackageName())));

        itemTitle=view.findViewById(R.id.item_title);

        itemLangue=view.findViewById(R.id.item_langue);
        itemMonnaie=view.findViewById(R.id.item_monnaie);
        itemPopulation=view.findViewById(R.id.item_population);
        itemSuperficie=view.findViewById(R.id.item_superficie);
        itemCapitale=view.findViewById(R.id.item_capitale);


        itemTitle.setText(countries[i].getName());

        itemLangue.setText(countries[i].getLanguage());
        itemMonnaie.setText(countries[i].getCurrency());
        itemPopulation.setText(String.valueOf(countries[i].getPopulation()));
        itemSuperficie.setText(String.valueOf(countries[i].getArea()));
        itemCapitale.setText(countries[i].getCapital());

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}