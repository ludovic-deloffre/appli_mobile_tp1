package com.example.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import static data.Country.countries;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

//        viewHolder.itemTitle.setText(titles[i]);
        viewHolder.itemTitle.setText(countries[i].getName());
//        viewHolder.itemDetail.setText(details[i]);
        viewHolder.itemCapitale.setText(countries[i].getCapital());
//        viewHolder.itemImage.setImageResource(images[i]);

        String uri = countries[i].getImgUri();

        Context c = viewHolder.itemImage.getContext();
        viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri , null , c.getPackageName())));




    }

    @Override
    public int getItemCount() {
        return countries.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemTitle;
        TextView itemCapitale;
//        TextView itemLangue;
//        TextView itemMonnaie;
//        TextView itemPopulation;
//        TextView itemSuperficie;


        ViewHolder(View itemView) {
            super(itemView);

        itemImage = itemView.findViewById(R.id.item_image);
        itemTitle = itemView.findViewById(R.id.item_title);
        itemCapitale = itemView.findViewById(R.id.item_capitale);
//        itemLangue = itemView.findViewById(R.id.item_langue);
//        itemMonnaie = itemView.findViewById(R.id.item_monnaie);
//        itemPopulation = itemView.findViewById(R.id.item_population);
//        itemSuperficie = itemView.findViewById(R.id.item_superficie);



        int position = getAdapterPosition();



            itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

                int position = getAdapterPosition();
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                //// Implementation with bundle
//                 Bundle bundle = new Bundle();
//                 bundle.putInt("country_id", position);
//                 Navigation.findNavController(v).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);

                //DetailFragmentArgs detailArgs = new DetailFragmentArgs();

                Bundle bundle = new Bundle();
                bundle.putInt("country_id", position);
                Navigation.findNavController(v).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);

                DetailFragmentArgs detailArgs =  DetailFragmentArgs.fromBundle(bundle);

                //DetailFragmentArgs detail = DetailFragmentArgs.build();
                int countryId = detailArgs.getCountryId();

                ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment(countryId);
                action.setCountryId(position);
                //Navigation.findNavController(v).navigate(action);

            }
        });

    }
    }

}